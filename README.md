# PBencoder - README #
---

### Overview ###

The **PBencoder** tool obfuscates strings for using them with the own decoding function in REALbasic projects.

### Screenshots ###

![PBencoder - Toolbar](development/readme/pbencoder1.png "PBencoder - Toolbar")

![PBencoder - Main window](development/readme/pbencoder2.png "PBencoder - Main window")

### Setup ###

* Start the **PBencoder.exe** executable with a doubleclick.
* Enter some text and press the button **Encode Input** to encode it.
* Copy the function call and insert it into your project.
* Insert the decoding function into your project.
* Please also check the user manual!

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBencoder** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
